# Proposition pour la causerie du 24 février 2018
([La Paillasse](http://lapaillasse.org), Paris)

Une causerie en forme d'atelier git, c'est-à-dire versionnage, truffé de teletype & d'atom, accompagné d'une imprimante et d'un pique-nique.
En partenariat avec "[Qu'est-ce que tu GEEKes ?](https://www.youtube.com/channel/UCWsf1on9hK_Aqh5jxJR3Q1Q)"

Pour participer : réservez par mail à bonsoir@orama.ninja & précisez ce que vous amènerez au pique-nique qui débutera à 11:30

- Pour partager ressources et idées pendant la causerie : => [le hackpad](https://hackmd.io/MYFgHGCGCcAmBsBaARsSZEmpAZo6ArAAx7IBM8kRAjNcAKZggDsQA===#) <=

### pour commencer, par exemple, il y aurait des matériaux...
"Chien blanc du nord, serpent noir du midi" (Récupéré dans l'Atelier de [Surfaces Utiles](http://surfaces-utiles.org/atelier.html))

* http://surfaces-utiles.org/chien-blanc-du-nord-serpent-noir-du-midi.html

Les matériaux qui ont servi à la création de l'oeuvre sont proposés ici comme un point de départ. Le projet est disponible dans ce dépôt Git, et de nouvelles branches peuvent être créées à loisir par les participants.


### les formes que ça pourrait prendre ensuite

* un mini-fanzine de quelques pages
* un site web
* n'importe quelle forme, en fait

### ressources Git
* [Git - site officiel](https://git-scm.com/)
* [Git petit guide](https://rogerdudler.github.io/git-guide/index.fr.html)
* [Pro Git book](https://git-scm.com/book/fr/v2)  (version FR)
* [Essayer Git]( https://try.github.io/levels/1/challenges/1)
* [Tuto Git](http://www-cs-students.stanford.edu/~blynn/gitmagic/intl/fr/ch01.html)
